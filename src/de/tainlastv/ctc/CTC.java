package de.tainlastv.ctc;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import de.tainlastv.ctc.commands.CTCCommand;
import de.tainlastv.ctc.listener.EntityExplodeListener;
import de.tainlastv.ctc.listener.PVPListener;
import de.tainlastv.ctc.listener.PlayerInteractListener;
import de.tainlastv.ctc.listener.PlayerJoinListener;
import de.tainlastv.ctc.utils.Game;
import de.tainlastv.ctc.utils.GameState;
import de.tainlastv.ctc.utils.Team;

public class CTC extends JavaPlugin {

	public static String prefix = "�8[�bCaptureTheCube�8] �f";
	private static CTC instance;
	
	public static File gameF = new File("plugins/CTC/game.yml");
	public static FileConfiguration gameC = YamlConfiguration.loadConfiguration(gameF);
	
	public static HashMap<String, Team> teams = new HashMap<>();
	public static HashMap<UUID, Team> playerTeams = new HashMap<>();
	
	public static GameState gameState = GameState.NOT_RUNNING;
	public Game game = new Game();
	
	@Override
	public void onEnable() {
		instance = this;
		Bukkit.getConsoleSender().sendMessage(prefix + "�aPlugin enabled");
		
		registerListener();
		registerCommands();
		
		loadTeams();
		loadPlayers();
		loadGame();
		game.run(this);
	}
	
	@Override
	public void onDisable() {
		Bukkit.getConsoleSender().sendMessage(prefix + "�cPlugin disabled");
		
		saveTeams();
		savePlayers();
		saveGame();
		
		CTC.saveConfig(gameF, gameC);
	}
	
	public void registerListener() {
		new PlayerInteractListener(this);
		new EntityExplodeListener(this);
		new PlayerJoinListener(this);
		new PVPListener(this);
	}
	
	public void registerCommands() {
		getCommand("ctc").setExecutor(new CTCCommand());
	}
	
	public static CTC getInstance() {
		return instance;
	}
	
	public void loadTeams() {
		if(!gameC.contains("teams.count"))
			return;
		
		for(int i = 0; i < gameC.getInt("teams.count"); i++) {
			String teamID = gameC.getString("teams.team" + i + ".id");
			String name = gameC.getString("teams.team" + i + ".name");
			String color = gameC.getString("teams.team" + i + ".color");
			double x = gameC.getDouble("teams.team" + i + ".spawn.x");
			double y = gameC.getDouble("teams.team" + i + ".spawn.y");
			double z = gameC.getDouble("teams.team" + i + ".spawn.z");
			float yaw = (float) gameC.getDouble("teams.team" + i + ".spawn.yaw");
			float pitch = (float) gameC.getDouble("teams.team" + i + ".spawn.pitch");
			World world = Bukkit.getWorld(gameC.getString("teams.team" + i + ".spawn.world"));
			
			Team team = new Team(teamID);
			team.setName(name);
			team.setColor(color);
			team.setSpawn(new Location(world, x, y, z, yaw, pitch));
			
			teams.put(teamID, team);
		}
	}
	
	public void saveTeams() {
		int count = 0;
		for(Entry<String, Team> entry : teams.entrySet()) {
			Team team = entry.getValue();
			String teamID = team.getId();
			
			gameC.set("teams.team" + count + ".id", teamID);
			gameC.set("teams.team" + count + ".name", team.getName());
			gameC.set("teams.team" + count + ".color", team.getColor());
			gameC.set("teams.team" + count + ".spawn.x", team.getSpawn().getX());
			gameC.set("teams.team" + count + ".spawn.y", team.getSpawn().getY());
			gameC.set("teams.team" + count + ".spawn.z", team.getSpawn().getZ());
			gameC.set("teams.team" + count + ".spawn.yaw", team.getSpawn().getYaw());
			gameC.set("teams.team" + count + ".spawn.pitch", team.getSpawn().getPitch());
			gameC.set("teams.team" + count + ".spawn.world", team.getSpawn().getWorld().getName());
			count++;
		}
		
		gameC.set("teams.count", count);
	}
	
	public void loadPlayers() {
		if(!gameC.contains("players.count"))
			return;
		
		for(int i = 0; i < gameC.getInt("players.count"); i++) {
			UUID uuid = UUID.fromString(gameC.getString("players.player" + i + ".uuid"));
			String team = gameC.getString("players.player" + i + ".team");
			
			if(!teams.containsKey(team))
				break;
			
			Team playerTeam = teams.get(team);
			playerTeam.addPlayer(uuid);
			
			teams.replace(team, playerTeam);
			
			playerTeams.put(uuid, playerTeam);
		}
	}
	
	public void savePlayers() {
		int count = 0;
		for(Entry<UUID, Team> entry : playerTeams.entrySet()) {
			UUID uuid = entry.getKey();
			Team team = entry.getValue();
			
			gameC.set("players.player" + count + ".uuid", uuid.toString());
			gameC.set("players.player" + count + ".team", team.getId());
			
			count++;
		}
		
		gameC.set("players.count", count);
	}
	
	public void loadGame() {
		if(gameC.contains("game.state")) {
			gameState = GameState.valueOf(gameC.getString("game.state"));
			
			if(gameState == GameState.PREPARING) {
				game.setPreparingCountdown(gameC.getInt("game.countdown"));
			} else if(gameState == GameState.START) {
				gameState = GameState.NOT_RUNNING;
			}
		}
	}
	
	public void saveGame() {
		gameC.set("game.state", gameState.toString());
		
		if(gameState == GameState.PREPARING) {
			gameC.set("game.countdown", game.getPreparingCountdown());
		}
	}
	
	public static void saveConfig(File f, FileConfiguration fc) {
		try {
			fc.options().copyDefaults();
			fc.save(f);
		} catch (IOException io) {
			io.printStackTrace();
		}
	}
}