package de.tainlastv.ctc.utils;

import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;

public class Team {

	private String id;
	private String name;
	private String color;
	private Location spawn;
	private boolean flag;
	private ArrayList<UUID> playerList;
	
	public Team(String teamID) {
		id = teamID;
		name = teamID;
		color = "&f";
		spawn = new Location(Bukkit.getWorld("world"), 0.0, 0.0, 0.0, (float) 0.0, (float) 0.0);
		flag = false;
		playerList = new ArrayList<>();
	}

	public String getId() {
		return id;
	}

	public void setId(String teamID) {
		id = teamID;
	}

	public String getName() {
		return name;
	}

	public void setName(String teamName) {
		name = teamName;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String teamColor) {
		color = teamColor;
	}

	public Location getSpawn() {
		return spawn;
	}

	public void setSpawn(Location teamSpawn) {
		spawn = teamSpawn;
	}

	public boolean hasFlag() {
		return flag;
	}
	
	public void setFlag(boolean hasFlag) {
		flag = hasFlag;
	}
	
	public ArrayList<UUID> getPlayerList() {
		return playerList;
	}

	public void setPlayerList(ArrayList<UUID> players) {
		playerList = players;
	}
	
	public void addPlayer(UUID uuid) {
		playerList.add(uuid);
	}
	
	public void removePlayer(UUID uuid) {
		playerList.remove(uuid);
	}
	
	public boolean containsPlayer(UUID uuid) {
		return playerList.contains(uuid);
	}
	
	public void clearPlayers() {
		playerList.clear();
	}
}