package de.tainlastv.ctc.utils;

import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import de.tainlastv.ctc.CTC;

public class Game {

	private int startCountdown = 11;
	private int preparingCountdown = 21;
	
	public void run(CTC ctc) {
		Bukkit.getScheduler().runTaskTimer(ctc, new Runnable() {
			public void run() {
				switch(CTC.gameState) {
				case NOT_RUNNING:
					return;
				case START:
					startCountdown--;
					
					for(Player p : Bukkit.getOnlinePlayers()) {
						p.setLevel(startCountdown);
						
						if((startCountdown % 10 == 0 && startCountdown != 0) || (startCountdown <= 10 && startCountdown > 1)) {
							p.sendMessage(CTC.prefix + "�aDas Spiel beginnt in �7" + startCountdown + " �aSekunden");
						} else if(startCountdown == 1) {
							p.sendMessage(CTC.prefix + "�aDas Spiel beginnt in �7" + startCountdown + " �aSekunde");
						} else if(startCountdown == 0) {
							p.sendMessage(CTC.prefix + "�aDas Spiel beginnt jetzt");
							
							if(CTC.playerTeams.containsKey(p.getUniqueId())) {
								Team team = CTC.playerTeams.get(p.getUniqueId());
								p.teleport(team.getSpawn());
							}
						}
					}
					
					if(startCountdown == 0) {
						CTC.gameState = GameState.PREPARING;
						startCountdown = 11;
					}
					
					break;
				case PREPARING:
					preparingCountdown--;
					
					for(Player p : Bukkit.getOnlinePlayers()) {
						if(preparingCountdown == 3600) {
							p.sendMessage(CTC.prefix + "�aDie Schutzphase endet in �760 �aMinuten");
						} else if(preparingCountdown == 1800) {
							p.sendMessage(CTC.prefix + "�aDie Schutzphase endet in �730 �aMinuten");
						} else if(preparingCountdown == 900) {
							p.sendMessage(CTC.prefix + "�aDie Schutzphase endet in �715 �aMinuten");
						} else if(preparingCountdown == 300) {
							p.sendMessage(CTC.prefix + "�aDie Schutzphase endet in �75 �aMinuten");
						} else if(preparingCountdown == 60) {
							p.sendMessage(CTC.prefix + "�aDie Schutzphase endet in �760 �aSekunden");
						} else if(preparingCountdown == 30) {
							p.sendMessage(CTC.prefix + "�aDie Schutzphase endet in �730 �aSekunden");
						} else if(preparingCountdown <= 10 & preparingCountdown > 1) {
							p.sendMessage(CTC.prefix + "�aDie Schutzphase endet in �7" + preparingCountdown + " �aSekunden");
						} else if(preparingCountdown == 1) {
							p.sendMessage(CTC.prefix + "�aDie Schutzphase endet in �7" + preparingCountdown + " �aSekunde");
						} else if(preparingCountdown == 0) {
							p.sendMessage(CTC.prefix + "�aDie Schutzphase endet jetzt");
						}
					}
					
					if(preparingCountdown == 0) {
						CTC.gameState = GameState.RUNNING;
						preparingCountdown = 21;
					}
					
					break;
				case RUNNING:
					break;
				}
			}
		}, 0L, 20L);
	}
	
	public void reset() {
		startCountdown = 11;
		preparingCountdown = 21;
		
		for(Entry<UUID, Team> entry : CTC.playerTeams.entrySet()) {
			Team team = entry.getValue();
			team.clearPlayers();
			
			CTC.teams.replace(team.getId(), team);
		}
		
		CTC.playerTeams.clear();
	}

	public int getPreparingCountdown() {
		return preparingCountdown;
	}
	
	public void setPreparingCountdown(int countdown) {
		preparingCountdown = countdown;
	}
}