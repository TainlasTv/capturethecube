package de.tainlastv.ctc.utils;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public enum CTCItem {

	BASE_SPAWNER(Material.GOLDEN_SHOVEL, "�bBase Spawner");
	
	private final ItemStack itemStack;
	
	private CTCItem(Material itemMaterial, String displayName) {
		itemStack = new ItemStack(itemMaterial);
		ItemMeta meta = itemStack.getItemMeta();
		meta.setDisplayName(displayName);
		itemStack.setItemMeta(meta);
	}
	
	public ItemStack getItemStack() {
		return itemStack;
	}
}