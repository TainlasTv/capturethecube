package de.tainlastv.ctc.utils;

public enum GameState {

	NOT_RUNNING, RUNNING, START, PREPARING;
}