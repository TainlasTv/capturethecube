package de.tainlastv.ctc.utils;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public enum CTCBlocks {

	BASE_BLACK(Material.BLACK_GLAZED_TERRACOTTA),
	BASE_PURPLE(Material.PURPLE_GLAZED_TERRACOTTA),
	BASE_LIGHT_GRAY(Material.LIGHT_GRAY_GLAZED_TERRACOTTA),
	BASE_GRAY(Material.GRAY_GLAZED_TERRACOTTA),
	BASE_LIGHT_BLUE(Material.LIGHT_BLUE_GLAZED_TERRACOTTA),
	BASE_CYAN(Material.CYAN_GLAZED_TERRACOTTA),
	BASE_BROWN(Material.BROWN_GLAZED_TERRACOTTA);
	
	private final ItemStack itemStack;
	private final Material material;
	
	private CTCBlocks(Material itemMaterial) {
		itemStack = new ItemStack(itemMaterial);
		material = itemMaterial;
	}
	
	public ItemStack getItemStack() {
		return itemStack;
	}
	
	public Material getMaterial() {
		return material;
	}
}