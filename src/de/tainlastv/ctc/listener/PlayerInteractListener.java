package de.tainlastv.ctc.listener;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Directional;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import de.tainlastv.ctc.CTC;
import de.tainlastv.ctc.utils.CTCBlocks;
import de.tainlastv.ctc.utils.CTCItem;

public class PlayerInteractListener implements Listener {

	public PlayerInteractListener(CTC ctc) {
		Bukkit.getPluginManager().registerEvents(this, ctc);
	}
	
	@EventHandler
	public void onInteract(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		
		if(p.getInventory().getItemInMainHand().equals(CTCItem.BASE_SPAWNER.getItemStack())) {
			e.setCancelled(true);
			
			if(e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
				spawnBlack(p, e.getClickedBlock().getLocation());
			}
			
		}
	}
	
	public void spawnBlack(Player p, Location blockLocation) {
		switch(p.getFacing()) {
		case NORTH:
			spawnBase(blockLocation, 1, -1, CTCBlocks.BASE_BLACK.getMaterial(), BlockFace.EAST, BlockFace.SOUTH, BlockFace.NORTH, BlockFace.WEST);
			break;
		case SOUTH:
			spawnBase(blockLocation, -1, 1, CTCBlocks.BASE_BLACK.getMaterial(), BlockFace.WEST, BlockFace.NORTH, BlockFace.SOUTH, BlockFace.EAST);
			break;
		case WEST:
			spawnBase(blockLocation, -1, -1, CTCBlocks.BASE_BLACK.getMaterial(), BlockFace.NORTH, BlockFace.WEST, BlockFace.EAST, BlockFace.SOUTH);
			break;
		case EAST:
			spawnBase(blockLocation, 1, 1, CTCBlocks.BASE_BLACK.getMaterial(), BlockFace.SOUTH, BlockFace.EAST, BlockFace.WEST, BlockFace.NORTH);
			break;
		default:
			break;
		}
		
		p.getInventory().setItemInMainHand(new ItemStack(Material.AIR));
	}
	
	public void spawnBase(Location blockLocation, int factorX, int factorZ, Material blockMaterial, BlockFace face1, BlockFace face2, BlockFace face3, BlockFace face4) {
		Location loc = blockLocation;
		BlockData blockData = loc.getBlock().getBlockData();
		Location enderLoc = blockLocation;
		
		int enderFactorX;
		int enderFactorZ;
		
		if(factorX == 1)
			enderFactorX = 1;
		else
			enderFactorX = 0;
		
		if(factorZ == 1)
			enderFactorZ = 1;
		else
			enderFactorZ = 0;
		
		
		for(int x = 0; x < 2; x++) {
			for(int z = 0; z < 2; z++) {
				loc = new Location(blockLocation.getWorld(), blockLocation.getX() + x * factorX, blockLocation.getY(), blockLocation.getZ() + z * factorZ);
				
				loc.getBlock().setType(blockMaterial);
				blockData = loc.getBlock().getBlockData();
				
				if(blockData instanceof Directional) {
					Directional directional = (Directional) blockData;
					if(x == 0 && z == 0) {
						directional.setFacing(face1);
					} else if(x == 0 && z == 1) {
						directional.setFacing(face2);
					} else if(x == 1 && z == 0) {
						directional.setFacing(face3);
					} else if(x == 1 && z == 1) {
						directional.setFacing(face4);
						enderLoc = new Location(blockLocation.getWorld(), blockLocation.getX() + enderFactorX, blockLocation.getY() + 1, blockLocation.getZ() + enderFactorZ);
						enderLoc.getWorld().spawnEntity(enderLoc, EntityType.ENDER_CRYSTAL);
					}
					loc.getBlock().setBlockData(directional);
				}
			}
		}
	}
}