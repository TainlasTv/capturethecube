package de.tainlastv.ctc.listener;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

import de.tainlastv.ctc.CTC;
import de.tainlastv.ctc.utils.GameState;
import de.tainlastv.ctc.utils.Team;

public class PVPListener implements Listener {

	public PVPListener(CTC ctc) {
		Bukkit.getPluginManager().registerEvents(this, ctc);
	}
	
	@EventHandler
	public void onPlayerHit(EntityDamageByEntityEvent e) {
		if(!(e.getEntity() instanceof Player && e.getDamager() instanceof Player))
			return;
		
		Player damager = (Player) e.getDamager();
		Player damaged = (Player) e.getEntity();
		
		if(!CTC.playerTeams.containsKey(damager.getUniqueId()) || !CTC.playerTeams.containsKey(damaged.getUniqueId()))
			e.setCancelled(true);
		else if(CTC.playerTeams.get(damager.getUniqueId()).getId().equals(CTC.playerTeams.get(damaged.getUniqueId()).getId()))
			e.setCancelled(true);
		else
			if(CTC.gameState != GameState.RUNNING)
				e.setCancelled(true);
	}
	
	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent e) {
		if(e.getEntity().getKiller() != null)
			e.setDeathMessage(CTC.prefix + "�aDer Spieler �7" + e.getEntity().getName() + " �astarb durch �7" + e.getEntity().getKiller().getName());
		else
			e.setDeathMessage(CTC.prefix + "�aDer Spieler �7" + e.getEntity().getName() + " �aist gestorben");
	}
	
	@EventHandler
	public void onPlayerRespawn(PlayerRespawnEvent e) {
		Player p = e.getPlayer();
		
		if(CTC.gameState != GameState.RUNNING)
			return;
		else if(!CTC.playerTeams.containsKey(p.getUniqueId()))
			return;
		
		Team playerTeam = CTC.playerTeams.get(p.getUniqueId());
		
		if(playerTeam.hasFlag())
			e.setRespawnLocation(playerTeam.getSpawn());
		else {
			p.setGameMode(GameMode.SPECTATOR);
			p.setSpectatorTarget(Bukkit.getPlayer(playerTeam.getPlayerList().get(0)));
		}
	}
}