package de.tainlastv.ctc.listener;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import de.tainlastv.ctc.CTC;

public class PlayerJoinListener implements Listener {

	public PlayerJoinListener(CTC ctc) {
		Bukkit.getPluginManager().registerEvents(this, ctc);
	}
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e) {
		if(!CTC.gameC.contains("spawns.lobby"))
			return;
		
		double x = CTC.gameC.getDouble("spawns.lobby.x");
		double y = CTC.gameC.getDouble("spawns.lobby.y");
		double z = CTC.gameC.getDouble("spawns.lobby.z");
		float yaw = (float) CTC.gameC.getDouble("spawns.lobby.yaw");
		float pitch = (float) CTC.gameC.getDouble("spawns.lobby.pitch");
		World world = Bukkit.getWorld(CTC.gameC.getString("spawns.lobby.world"));
		
		Location loc = new Location(world, x, y, z, yaw, pitch);
		
		e.getPlayer().teleport(loc);
	}
}