package de.tainlastv.ctc.listener;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

import de.tainlastv.ctc.CTC;

public class EntityExplodeListener implements Listener {

	public EntityExplodeListener(CTC ctc) {
		Bukkit.getPluginManager().registerEvents(this, ctc);
	}
	
	@EventHandler
	public void onEntityExplode(EntityExplodeEvent e) {
		if(e.getEntityType().equals(EntityType.ENDER_CRYSTAL) || e.getEntityType().equals(EntityType.PRIMED_TNT)) {
			e.setCancelled(true);
			e.setYield(0);
		}
	}
	
	@EventHandler
	public void onWeatherChange(WeatherChangeEvent e) {
		if(e.toWeatherState()) {
			e.setCancelled(true);			
		}
	}
	
	@EventHandler
	public void onEntityDamage(EntityDamageEvent e) {
		if(e.getEntityType().equals(EntityType.ENDER_CRYSTAL)) {
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onEntityDamageByEntity(EntityDamageByEntityEvent e) {
		if(e.getEntityType().equals(EntityType.ENDER_CRYSTAL)) {
			e.setCancelled(true);
			
			if(e.getDamager().getType().equals(EntityType.PLAYER)) {
				e.getEntity().remove();
			}
		}
	}
	
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent e) {
		if(e.getBlock().getType().equals(Material.TNT)) {
			e.getBlock().setType(Material.AIR);
		}
	}
}