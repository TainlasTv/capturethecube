package de.tainlastv.ctc.commands.ctc;

import org.bukkit.command.CommandSender;

import de.tainlastv.ctc.CTC;
import de.tainlastv.ctc.utils.GameState;

public class ResetCommand {

	private CommandSender sender;
	
	public ResetCommand(CommandSender cs) {
		sender = cs;
		
		execute();
	}
	
	public void execute() {
		if(!sender.hasPermission("ctc.commands.reset")) {
			sender.sendMessage(CTC.prefix + "�4Du hast nicht die n�tige Berechtigung, um diesen Befehl ausf�hren zu d�rfen");
			return;
		}
		
		if(CTC.gameState != GameState.NOT_RUNNING) {
			sender.sendMessage(CTC.prefix + "�4Das Spiel l�uft noch");
			return;
		}
		
		CTC.getInstance().game.reset();
		
		sender.sendMessage(CTC.prefix + "�aDu hast die Spieldaten zur�ckgesetzt");
	}
}