package de.tainlastv.ctc.commands.ctc;

import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.tainlastv.ctc.CTC;

public class SetSpawnCommand {

	private CommandSender sender;
	private String[] args;
	
	public SetSpawnCommand(CommandSender cs, String[] arguments) {
		sender = cs;
		args = arguments;
		execute();
	}
	
	public void execute() {
		if(!(sender instanceof Player)) {
			sender.sendMessage(CTC.prefix + "�4Du musst ein Spieler sein, um diesen Befehl ausf�hren zu d�rfen");
			return;
		}
		
		if(!sender.hasPermission("ctc.commands.setspawn")) {
			sender.sendMessage(CTC.prefix + "�4Du hast nicht die n�tige Berechtigung, um diesen Befehl ausf�hren zu d�rfen");
			return;
		}
		
		Player p = (Player) sender;
		Location loc = p.getLocation();
		
		if(args[1].equalsIgnoreCase("lobby")) {
			CTC.gameC.set("spawns.lobby.x", loc.getX());
			CTC.gameC.set("spawns.lobby.y", loc.getY());
			CTC.gameC.set("spawns.lobby.z", loc.getZ());
			CTC.gameC.set("spawns.lobby.yaw", loc.getYaw());
			CTC.gameC.set("spawns.lobby.pitch", loc.getPitch());
			CTC.gameC.set("spawns.lobby.world", loc.getWorld().getName());
			
			p.sendMessage(CTC.prefix + "�aDer Lobby-Spawn wurde neu gesetzt");
			return;
		} else if(args[1].equalsIgnoreCase("spectator")) {
			CTC.gameC.set("spawns.spectator.x", loc.getX());
			CTC.gameC.set("spawns.spectator.y", loc.getY());
			CTC.gameC.set("spawns.spectator.z", loc.getZ());
			CTC.gameC.set("spawns.spectator.yaw", loc.getYaw());
			CTC.gameC.set("spawns.spectator.pitch", loc.getPitch());
			CTC.gameC.set("spawns.spectator.world", loc.getWorld().getName());
			
			p.sendMessage(CTC.prefix + "�aDer Spectator-Spawn wurde neu gesetzt");
			return;
		}
		
		sender.sendMessage(CTC.prefix + "�4Falsche Benutzung");
		new HelpCommand(sender);
	}
}