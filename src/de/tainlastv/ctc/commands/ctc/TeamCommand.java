package de.tainlastv.ctc.commands.ctc;

import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.tainlastv.ctc.CTC;
import de.tainlastv.ctc.utils.GameState;
import de.tainlastv.ctc.utils.Team;

public class TeamCommand {

	private CommandSender sender;
	private String[] args;
	
	public TeamCommand(CommandSender cs, String[] arguments) {
		sender = cs;
		args = arguments;
		execute();
	}
	
	public void execute() {
		if(CTC.gameState != GameState.NOT_RUNNING) {
			sender.sendMessage(CTC.prefix + "�4Du kannst diesen Befehl nicht nutzen, w�hrend das Spiel l�uft");
			return;
		}
		
		if(args.length == 3) {
			if(args[1].equalsIgnoreCase("create")) {
				if(!sender.hasPermission("ctc.commands.team.create")) {
					sender.sendMessage(CTC.prefix + "�4Du hast nicht die n�tige Berechtigung, um diesen Befehl ausf�hren zu d�rfen");
					return;
				}
				
				String teamID = args[2].toLowerCase();
				
				if(CTC.teams.containsKey(teamID)) {
					sender.sendMessage(CTC.prefix + "�4Es existiert bereits ein Team mit der ID �7" + teamID);
					return;
				}
				
				Team team = new Team(teamID);
				
				CTC.teams.put(teamID, team);
				
				sender.sendMessage(CTC.prefix + "�aDas Team mit der ID �7" + teamID + " �awurde erstellt");
				return;
			} else if(args[1].equalsIgnoreCase("setspawn")) {
				if(!(sender instanceof Player)) {
					sender.sendMessage(CTC.prefix + "�4Du musst ein Spieler sein, um diesen Befehl ausf�hren zu d�rfen");
					return;
				}
				
				if(!sender.hasPermission("ctc.commands.team.setspawn")) {
					sender.sendMessage(CTC.prefix + "�4Du hast nicht die n�tige Berechtigung, um diesen Befehl ausf�hren zu d�rfen");
					return;
				}
				
				String teamID = args[2].toLowerCase();
				Player p = (Player) sender;
				Location loc = p.getLocation();
				
				if(!CTC.teams.containsKey(teamID)) {
					p.sendMessage(CTC.prefix + "�4Es existiert kein Team mit der ID �7" + teamID);
					return;
				}
				
				Team team = CTC.teams.get(teamID);
				team.setSpawn(loc);
				
				CTC.teams.replace(teamID, team);
				
				p.sendMessage(CTC.prefix + "�aDer Spawn f�r das Team mit der ID �7" + teamID + " �awurde gesetzt");
				return;
			} else if(args[1].equalsIgnoreCase("join")) {
				if(!(sender instanceof Player)) {
					sender.sendMessage(CTC.prefix + "�4Du musst ein Spieler sein, um diesen Befehl ausf�hren zu d�rfen");
					return;
				}
				
				String teamID = args[2].toLowerCase();
				Player p = (Player) sender;
				UUID uuid = p.getUniqueId();
				
				if(!CTC.teams.containsKey(teamID)) {
					p.sendMessage(CTC.prefix + "�4Es existiert kein Team mit der ID �7" + teamID);
					return;
				} else if(CTC.playerTeams.containsKey(uuid) && CTC.playerTeams.get(uuid).getId().equals(teamID)) {
					p.sendMessage(CTC.prefix + "�4Du bist bereits in diesem Team");
					return;
				}
				
				
				Team newTeam = CTC.teams.get(teamID);
				newTeam.addPlayer(uuid);
				CTC.teams.replace(teamID, newTeam);
				
				if(CTC.playerTeams.containsKey(uuid)) {
					Team oldTeam = CTC.playerTeams.get(uuid);
					oldTeam.removePlayer(uuid);
					CTC.teams.replace(oldTeam.getId(), oldTeam);
					
					CTC.playerTeams.replace(uuid, newTeam);
				} else {
					CTC.playerTeams.put(uuid, newTeam);
				}
				
				p.sendMessage(CTC.prefix + "�aDu bist dem Team " + ChatColor.translateAlternateColorCodes('&', newTeam.getColor() + newTeam.getName()) + " �abeigetreten");
				return;
			}
		} else if(args.length == 4) {
			if(args[1].equalsIgnoreCase("setname")) {
				String teamID = args[2].toLowerCase();
				
				if(!sender.hasPermission("ctc.commands.team.setname")) {
					sender.sendMessage(CTC.prefix + "�4Du hast nicht die n�tige Berechtigung, um diesen Befehl ausf�hren zu d�rfen");
					return;
				}
				
				if(!CTC.teams.containsKey(teamID)) {
					sender.sendMessage(CTC.prefix + "�4Es existiert kein Team mit der ID �7" + teamID);
					return;
				}
				
				String teamName = args[3];
				
				Team team = CTC.teams.get(teamID);
				team.setName(teamName);
				
				CTC.teams.replace(teamID, team);
				
				sender.sendMessage(CTC.prefix + "�aDer Name f�r das Team mit der ID �7" + teamID + " �awurde in �7" + teamName + " �age�ndert");
				return;
			} else if(args[1].equalsIgnoreCase("setcolor")) {
				String teamID = args[2].toLowerCase();
				
				if(!sender.hasPermission("ctc.commands.team.setcolor")) {
					sender.sendMessage(CTC.prefix + "�4Du hast nicht die n�tige Berechtigung, um diesen Befehl ausf�hren zu d�rfen");
					return;
				}
				
				if(!CTC.teams.containsKey(teamID)) {
					sender.sendMessage(CTC.prefix + "�4Es existiert kein Team mit der ID �7" + teamID);
					return;
				}
				
				String teamColor = args[3];
				
				Team team = CTC.teams.get(teamID);
				team.setColor(teamColor);
				
				CTC.teams.replace(teamID, team);
				
				sender.sendMessage(CTC.prefix + "�aDie Farbe f�r das Team mit der ID �7" + teamID + " �awurde ge�ndert");
				return;
			}
		}

		sender.sendMessage(CTC.prefix + "�4Falsche Benutzung");
		new HelpCommand(sender);
	}
}