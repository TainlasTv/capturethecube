package de.tainlastv.ctc.commands.ctc;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.tainlastv.ctc.CTC;
import de.tainlastv.ctc.utils.CTCItem;

public class GetItemsCommand {

	private CommandSender sender;
	
	public GetItemsCommand(CommandSender cs) {
		sender = cs;
		execute();
	}
	
	public void execute() {
		if(!(sender instanceof Player)) {
			sender.sendMessage(CTC.prefix + "�4Du musst ein Spieler sein, um diesen Befehl ausf�hren zu d�rfen");
			return;
		} else if(!sender.hasPermission("ctc.commands.giveitems")) {
			sender.sendMessage(CTC.prefix + "�4Du hast nicht die n�tige Berechtigung, um diesen Befehl ausf�hren zu d�rfen");
			return;
		}
		
		Player p = (Player) sender;
		
		p.getInventory().addItem(CTCItem.BASE_SPAWNER.getItemStack());
		
		p.sendMessage(CTC.prefix + "�aDu hast alle n�tigen Items f�r CaptureTheCube erhalten");
	}
}