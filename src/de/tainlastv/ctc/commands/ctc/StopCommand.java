package de.tainlastv.ctc.commands.ctc;

import org.bukkit.command.CommandSender;

import de.tainlastv.ctc.CTC;
import de.tainlastv.ctc.utils.GameState;

public class StopCommand {

	private CommandSender sender;
	
	public StopCommand(CommandSender cs) {
		sender = cs;
		
		execute();
	}
	
	public void execute() {
		if(!sender.hasPermission("ctc.commands.stop")) {
			sender.sendMessage(CTC.prefix + "�4Du hast nicht die n�tige Berechtigung, um diesen Befehl ausf�hren zu d�rfen");
			return;
		}
		
		if(CTC.gameState == GameState.NOT_RUNNING) {
			sender.sendMessage(CTC.prefix + "�4Das Spiel l�uft nicht");
			return;
		}
		
		CTC.gameState = GameState.NOT_RUNNING;
		
		sender.sendMessage(CTC.prefix + "�aDu hast das Spiel gestoppt");
	}
}