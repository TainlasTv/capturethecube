package de.tainlastv.ctc.commands.ctc;

import org.bukkit.command.CommandSender;

import de.tainlastv.ctc.CTC;

public class HelpCommand {

	private CommandSender sender;
	
	public HelpCommand(CommandSender cs) {
		sender = cs;
		execute();
	}
	
	public void execute() {
		sender.sendMessage(CTC.prefix + "�b=== �7CaptureTheCube - Command List �b===");
		sender.sendMessage("�7/ctc getitems");
		sender.sendMessage("�7/ctc team create <team_id>");
		sender.sendMessage("�7/ctc team setname <team_id> <name>");
		sender.sendMessage("�7/ctc team setcolor <team_id> <color_code>");
		sender.sendMessage("�7/ctc team setspawn <team_id>");
		sender.sendMessage("�7/ctc team join <team_id>");
		sender.sendMessage("�7/ctc setspawn <lobby/spectator>");
		sender.sendMessage(CTC.prefix + "�b=== �7CaptureTheCube - Command List �b===");
	}
}