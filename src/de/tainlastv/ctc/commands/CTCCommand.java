package de.tainlastv.ctc.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import de.tainlastv.ctc.CTC;
import de.tainlastv.ctc.commands.ctc.GetItemsCommand;
import de.tainlastv.ctc.commands.ctc.HelpCommand;
import de.tainlastv.ctc.commands.ctc.ResetCommand;
import de.tainlastv.ctc.commands.ctc.SetSpawnCommand;
import de.tainlastv.ctc.commands.ctc.StartCommand;
import de.tainlastv.ctc.commands.ctc.StopCommand;
import de.tainlastv.ctc.commands.ctc.TeamCommand;

public class CTCCommand implements CommandExecutor {
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(args.length == 0) {
			new HelpCommand(sender);
			return true;
		} else if(args.length == 1) {
			if(args[0].equalsIgnoreCase("getitems")) {
				new GetItemsCommand(sender);
				return true;
			} else if(args[0].equalsIgnoreCase("start")) {
				new StartCommand(sender);
				return true;
			} else if(args[0].equalsIgnoreCase("stop")) {
				new StopCommand(sender);
				return true;
			} else if(args[0].equalsIgnoreCase("reset")) {
				new ResetCommand(sender);
				return true;
			}
		} else if(args.length == 2) {
			if(args[0].equalsIgnoreCase("setspawn")) {
				new SetSpawnCommand(sender, args);
				return true;
			}
		} else if(args.length == 3) {
			if(args[0].equalsIgnoreCase("team")) {
				new TeamCommand(sender, args);
				return true;
			}
		} else if(args.length == 4) {
			if(args[0].equalsIgnoreCase("team")) {
				new TeamCommand(sender, args);
				return true;
			}
		}
		
		sender.sendMessage(CTC.prefix + "�4Falsche Benutzung");
		new HelpCommand(sender);
		return true;
	}
}